# arcospyu

[![pipeline status](https://gitlab.com/arcoslab/arcospyu/badges/master/pipeline.svg)](https://gitlab.com/arcoslab/arcospyu/commits/master)


## arcospyu: Arcoslab Python Utils.

This is a library where we keep a bunch of common functionalities that are required by many
Python projects at ARCOS-Lab.

We are working on a refactor of this library. **Python2 support will be dropped** and we will
introduce **many API changes**. We are not keeping backwards compatibility. We are also going to
stop using Makefiles for installing and use more standard/Pythonic tools so that this library can
become *pip installable*.

Read the CHANGELOG for more information about the changes we are planning for version 0.2.0